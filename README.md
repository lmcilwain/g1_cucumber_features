# Gold Cucumber Feature Tests

## Description
Example of how to write tests using cucumber.

Feature is defined in a *.feature file
The associated step definitions are in an associated *.rb file
Helper methods are created in features/support directory

## Installation
The best way to use this is with a ruby version manager. Either RVM or
RBENV. Install the version or ruby defined in the Gemfile.
## How to use

Clone the repo
```
git@bitbucket.org:lmcilwain/g1_cucumber_features.git
```

Bundle install the gems from the root directory
```
bundle
```

Run cucumber
```
cucumber
```

## References
* http://www.rvm.io
* https://github.com/rbenv/rbenv
* https://cucumber.io/docs/reference
* https://github.com/cucumber/cucumber-ruby
* https://github.com/jnicklas/capybara
* https://github.com/rspec/rspec-expectations
