Feature: Add a nc admin site
  In order to create a site
  As an nc administrator
  I want to go through G2
  @javascript @nc
  Scenario: A nc admin can create a site
    Given a nc admin visits g2
    And logs in with valid nc admin credentials
    And creates a nc site
    When site is searched
    Then site link shows
