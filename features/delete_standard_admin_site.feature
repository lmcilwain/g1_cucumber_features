Feature: delete a standard admin site
  In order to delete a site
  As a standard administrator
  I want to go through G2
  @javascript @standard
  Scenario: A standard admin can delete a site
    Given a standard admin visits g2
    And logs in with valid credentials
    And creates a standard site
    When site is deleted
    Then site link does not show
