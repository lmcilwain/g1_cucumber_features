module LoginHelpers
  def login_with_username(username='789617')
    visit 'https://tsiweb4.teachingstrategies.com/gold/teachers/checkpointDates.cfm'
    fill_in 'username', with: username
    fill_in 'password', with: 'Asdf1234'
    click_button 'Submit'
  end


  def admin_login(username, password)
    fill_in 'username', with: username
    fill_in 'password', with: password
    click_button 'Sign in'
  end
end

World(LoginHelpers)
