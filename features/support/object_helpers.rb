module ObjectHelpers
  def site_name
    @site_name ||= Faker::Company.name
  end
end
World(ObjectHelpers)
