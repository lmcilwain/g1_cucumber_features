require 'capybara/cucumber'
require 'faker'
require 'simplecov'
SimpleCov.start
Capybara.app='Gold'

CONFIG = YAML.load(File.read(File.expand_path('../config.yml', __FILE__)))

# Additional Rspec configurations if needed
# Must have rspec-core bundled in the Gemfile
# RSpec.configure do |config|
#  config.expect_with :rspec do |c|
#    c.syntax = :expect
#  end
# end
