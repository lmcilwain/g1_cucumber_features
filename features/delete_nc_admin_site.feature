Feature: delete a nc admin site
  In order to delete a site
  As a nc administrator
  I want to go through G2
  @javascript @nc
  Scenario: A nc admin can delete a site
    Given a nc admin visits g2
    And logs in with valid nc admin credentials
    And creates a nc site
    When site is deleted
    Then site link does not show
