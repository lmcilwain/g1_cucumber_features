Feature: Winter checkpoint
  @javascript
  Scenario: Documentation is set to winter checkpoint
    Given a valid user
    And goes to the documentation section for a class
    And selects a child
    And sets an observation date within the winter checkpoint period
    And a note is added
    When the save and review button is clicked
    Then the documentation is added to the winter checkpoint period
