Feature: Add a standard admin site
  In order to create a site
  As a standard administrator
  I want to go through G2
  @javascript @standard
  Scenario: A standard admin can create a site
    Give a standard admin visits g2
    And logs in with valid credentials
    And creates a standard site
    When site is searched
    Then site link shows
