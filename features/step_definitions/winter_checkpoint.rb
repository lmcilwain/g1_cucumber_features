Given(/^a valid user$/) do
  login_with_username
end

Given(/^goes to the documentation section for a class$/) do
  goto_documentation_for_class
 end

Given(/^selects a child$/) do
  select_child_with_value
end

Given(/^sets an observation date within the winter checkpoint period$/) do
  set_observation_date_to
end

Given(/^a note is added$/) do
  select_documentation_note
end

When(/^the save and review button is clicked$/) do
  save_and_review
end

Then(/^the documentation is added to the winter checkpoint period$/) do
  expect(page).to have_content 'Observation Added to Winter 2015/2016'
end
