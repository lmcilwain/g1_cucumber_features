Given(/^site is deleted$/) do
  search_site site_name
  delete_site site_name
  seconds_to_pause(2)
end

Then(/^site link does not show$/) do
  expect(page).to_not have_link site_name
end
