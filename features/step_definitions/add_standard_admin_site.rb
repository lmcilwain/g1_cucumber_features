Given(/^a standard admin visits g2$/) do
  visit CONFIG['g2_alpha_url']
end

Given(/^logs in with valid credentials$/) do
  admin_login CONFIG['admin']['username'], CONFIG['admin']['password']
end

Given(/^creates a standard site$/) do
  create_standard_site site_name
end

When(/^site is searched$/) do
  search_site site_name
end

Then(/^site link shows$/) do
  expect(page).to have_link site_name
end
