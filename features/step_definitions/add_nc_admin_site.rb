Given(/^a nc admin visits g(\d+)$/) do |arg1|
  visit CONFIG['g2_alpha_url']
end

Given(/^logs in with valid nc admin credentials$/) do
  admin_login CONFIG['k3admin']['username'], CONFIG['k3admin']['password']
end

Given(/^creates a nc site$/) do
  create_nc_site site_name
end
